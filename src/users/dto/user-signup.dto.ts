import { IS_EMPTY, IS_NOT_EMPTY, IsEmail, IsEmpty, IsEnum, IsNotEmpty, IsNumber, IsString, MaxLength, ValidateIf, isEmail, isEmpty } from '@nestjs/class-validator';
import { UserTypes } from '../entities/users.entity';
import { IsStrongPassword } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UserSignupDto {

    @ApiProperty({ name: 'user_type', enum: UserTypes })
    @IsEnum(UserTypes)
    readonly user_type: UserTypes;

    @ApiProperty()
    @IsNumber()
    readonly member_id: number;

    @ApiProperty()
    @IsString()
    @IsEmail()
    readonly email: string;

    //confirmpassword?
    @ApiProperty() //description?
    @IsString()
    @IsStrongPassword() //Error message?
    readonly password: string;

    @ApiProperty()
    @IsString()
    readonly mobile: string;

    @ApiProperty()
    @IsString()
    readonly first_name: string;

    @ApiProperty()
    @IsString()
    readonly last_name: string;

    @ApiProperty()
    @ValidateIf(o => o.user_type === UserTypes.COMPANY)
    @IsString()
    @IsNotEmpty()
    readonly company_name?: string;

    @ApiProperty()
    @ValidateIf(o => o.user_type === UserTypes.COMPANY)
    @IsString()
    @IsNotEmpty()
    readonly company_license?: string;

    @ApiProperty()
    @ValidateIf(o => o.user_type === UserTypes.COMPANY)
    @IsString()
    @IsNotEmpty()
    readonly trn_number?: string;
}
