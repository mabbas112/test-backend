import { Body, Controller, Get, Inject, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { UsersService } from './users.service';
import { UserSignupDto } from './dto/user-signup.dto';

@Controller('users')
@ApiTags("Users")
export class UsersController {
    constructor(private readonly userService: UsersService) { }

    @Post()
    @UsePipes(ValidationPipe)
    signup(@Body() userSignupDto: UserSignupDto) {
        // 
        console.log(userSignupDto);
        return this.userService.createUser(userSignupDto);
    }

    @Get()
    getAllUsers() {
        return this.userService.findAllUser();
    }
}
